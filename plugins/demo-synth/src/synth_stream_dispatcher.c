

#include "synth_stream_dispatcher.h"


apt_bool_t synth_init_stream_dispatcher()
{
	if(pthread_mutex_init(&mutex, NULL) != 0)
	{
		apt_log(APT_LOG_MARK,APT_PRIO_ERROR,"Init mutex failure");
		return FALSE;
	}

	return TRUE;
}

void *synth_stream_send_msg(void* data)
{
	apt_log(APT_LOG_MARK,APT_PRIO_DEBUG,"Stream send request Entering");
	synth_stream_data_t *stream_data = (synth_stream_data_t*)data;
	
	if(!send_http_request(stream_data->url, stream_data->tts_response, stream_data->len))
	{
		apt_log(APT_LOG_MARK,APT_PRIO_ERROR,"Thread send http request failure");
	}
	
	if(!send_mrcp_inprogress(stream_data->mrcp_channel, stream_data->mrcp_request, stream_data->mrcp_response))
	{
		apt_log(APT_LOG_MARK,APT_PRIO_ERROR,"Send mrcp inprogress failure");
	}

	pthread_mutex_lock(&mutex);
	stream_count--;
	pthread_mutex_unlock(&mutex);

	if(data)
	{
		free(data);
		data = NULL;
	}
	apt_log(APT_LOG_MARK,APT_PRIO_DEBUG,"Stream send request Leaving");
}

apt_bool_t synth_stream_dispatcher(synth_stream_data_t *stream_data)
{
	apt_log(APT_LOG_MARK,APT_PRIO_DEBUG,"Dispatcher stream Entering");
	
	if(stream_count >= SYNTH_STREAM_MAX)
	{
		apt_log(APT_LOG_MARK,APT_PRIO_WARNING,"Stream is maximum %d", stream_count);	
		
		return FALSE;
	}
	
	pthread_t p = NULL;
	pthread_mutex_lock(&mutex);
	stream_count++;	
	pthread_mutex_unlock(&mutex);
	apt_log(APT_LOG_MARK,APT_PRIO_DEBUG,"Number of stream running is %d", stream_count);
	if(pthread_create(&p, NULL, synth_stream_send_msg, stream_data) != 0)
	{
		apt_log(APT_LOG_MARK,APT_PRIO_ERROR,"Can't create thread");
		pthread_mutex_lock(&mutex);
		stream_count--;	
		pthread_mutex_unlock(&mutex);
		return FALSE;
	}
	
//	pthread_join(p, NULL);
	
	apt_log(APT_LOG_MARK,APT_PRIO_DEBUG,"Dispatcher stream Leaving");
	return TRUE;
}

