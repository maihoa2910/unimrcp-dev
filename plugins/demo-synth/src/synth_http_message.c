


#include "synth_http_message.h"
#include "synth_stream_dispatcher.h"
#include "apt_log.h"




apt_bool_t synth_http_send_request_msg (char *requestURL, void **response, int *length)
{
	CURL *curl;
	char received_msg[SYNTH_MSG_MAX_DATA_LENGTH] = "";
	char *point_msg = received_msg;
	char *first_msg = received_msg;
	long int response_code = 0;

	curl = curl_easy_init();


	if(!requestURL)
	{
		apt_log(APT_LOG_MARK,APT_PRIO_ERROR,"Input is null");
		return FALSE;
	}

	if(curl)
	{
		curl_easy_setopt(curl, CURLOPT_URL, requestURL);
		curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);
		curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

		struct curl_slist *chunk = NULL;
		chunk = curl_slist_append(chunk, "Accept: */*");
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);

		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, read_data_from_http_message);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &point_msg);
		curl_easy_setopt(curl, CURLOPT_TIMEOUT, 60);

		if(CURLE_OK != curl_easy_perform(curl))
		{
			apt_log(APT_LOG_MARK,APT_PRIO_ERROR,"Send request failure");
			return FALSE;
		}

		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);

		apt_log(APT_LOG_MARK,APT_PRIO_DEBUG,"Response Code from TTS = %d", response_code);
		if(response_code == 200 && first_msg[0] != '\0')
		{
			if(!synth_process_response_get_audio(first_msg, response, length))
			{
				apt_log(APT_LOG_MARK,APT_PRIO_ERROR,"Parse data failure");
				return FALSE;
			}
		}
		else
		{
			apt_log(APT_LOG_MARK,APT_PRIO_ERROR,"Request timeout");
			return FALSE;
		}

	}
	else
	{
		apt_log(APT_LOG_MARK,APT_PRIO_ERROR,"Init curl failure");
		return FALSE;
	}
	
	return TRUE;
}

apt_bool_t synth_send_request_get_audio (char *text_input, synth_message_info_t msg_info, void *data_stream_i)
{
	char requestURL[5000];

	// Build request
	if(!synth_build_request_get_audio(requestURL, msg_info, text_input))
	{
		apt_log(APT_LOG_MARK,APT_PRIO_ERROR,"Build request failure");
		return FALSE;
	}

	synth_stream_data_t *data_stream = (synth_stream_data_t*) data_stream_i;
	strcpy(data_stream->url, requestURL);
	
	if(!synth_stream_dispatcher(data_stream)) 
	{
		apt_log(APT_LOG_MARK,APT_PRIO_ERROR,"Dispatcher stream failure");
		return FALSE;
	}

#if 0
	
	// Send request
	if(!synth_http_send_request_msg(requestURL,response, length))
	{
		apt_log(APT_LOG_MARK,APT_PRIO_ERROR,"Request tss failure");
		return FALSE;
	}
#endif

	return TRUE;
	
}

apt_bool_t synth_build_request_get_audio (char *requestURL, synth_message_info_t msg_info, char *text_input)
{
	//const char* tempTest = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\" xmlns=\"http://www.w3.org/2001/10/synthesis\" xml:lang=\"vi\">b?m ph�m 1 d? nghe ti?ng anh</speak>";
	sprintf(requestURL, SYNTH_MSG_REQUEST_TMP,  msg_info.url, 
												msg_info.port, 
												msg_info.input_type, 
												curl_easy_escape(NULL, text_input, strlen(text_input)), 
												msg_info.output_type, 
												msg_info.audio, 
												msg_info.locale, 
												msg_info.voice);
	
	apt_log(APT_LOG_MARK,APT_PRIO_DEBUG,"Request URL: %s", requestURL);
	return TRUE;
}

apt_bool_t synth_process_response_get_audio (char *receive_data, void **response, int *length)
{
	audio_wave_header_fmt_t *wave_header;
	wave_header = (audio_wave_header_fmt_t *) receive_data;
	apt_log(APT_LOG_MARK,APT_PRIO_INFO,"Length of audio data received: %d", wave_header->length);
	if(!(*response)) {
		unsigned int llength;
		llength = wave_header->length < SYNTH_MSG_MAX_DATA_LENGTH?wave_header->length : SYNTH_MSG_MAX_DATA_LENGTH;
		(*response) = malloc(llength);
		if(!(*response)) {
			apt_log(APT_LOG_MARK,APT_PRIO_ERROR,"Allocate memory for audio response failure");
			return FALSE;
		}
		memcpy((*response), receive_data, llength);
		g_memory += llength;
		apt_log(APT_LOG_MARK,APT_PRIO_INFO,"Total mem allocate +%d = %d", llength, g_memory);
		*length = llength;
	}
	else {
		apt_log(APT_LOG_MARK,APT_PRIO_ERROR,"Response pointer not null (require null)");
		return FALSE;
	}

	return TRUE;
}

apt_bool_t synth_dispatcher_http_request (char *addr, char *port, char *input_type, char *output_type, char *audio, char *locale, char * voice, synth_message_info_t * msg_info)
{
	/* .............. Chon server gui request*/



	if(addr)			strcpy(msg_info->url, addr);
	else			strcpy(msg_info->url, SYNTH_MSG_DEFAULT_ADDR_TTS);		

	if(port)		strcpy(msg_info->port, port);
	else			strcpy(msg_info->port, SYNTH_MSG_DEFAULT_PORT_TTS);

	if(input_type)	strcpy(msg_info->input_type, input_type);
	else			strcpy(msg_info->input_type, SYNTH_MSG_INPUT_TYPE_SSML);

	if(output_type)	strcpy(msg_info->output_type, output_type);
	else			strcpy(msg_info->output_type, SYNTH_MSG_OUTPUT_TYPE);

	if(audio)		strcpy(msg_info->audio, audio);
	else 			strcpy(msg_info->audio, SYNTH_MSG_OUTPUT_AUDIO_WAVE_FILE);

	if(locale)		strcpy(msg_info->locale, locale);
	else			strcpy(msg_info->locale, SYNTH_MSG_LOCALE_VI);

	if(voice)		strcpy(msg_info->voice, voice);
	else 			strcpy(msg_info->voice, SYNTH_MSG_VOICE_TVH);
	
	return TRUE;
}

size_t read_data_from_http_message (void *ptr, size_t size, size_t nmemb, void **userdata)
{
	size_t new_len = size*nmemb;

	//HTTP message response have data body
	if(new_len > 0)
	{
		//copy all data received to process later
		memcpy((char *)(*userdata), ptr, new_len);
		((char *)(*userdata))[new_len] = '\0';
	}
	(*userdata) += new_len;

	return new_len;
}





