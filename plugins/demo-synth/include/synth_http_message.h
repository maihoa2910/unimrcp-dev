
#ifndef SYNTH_HTTP_MESSAGE_H_
#define SYNTH_HTTP_MESSAGE_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
#include <stdint.h>

#include "synth_stream_dispatcher.h"
#include "apt_log.h"

/* default address, port */
#define SYNTH_MSG_DEFAULT_ADDR_TTS 			"http://tts.amobi.vn"
#define SYNTH_MSG_DEFAULT_PORT_TTS			"59125"

/* Input type */
#define SYNTH_MSG_INPUT_TYPE_TEXT "TEXT"
#define SYNTH_MSG_INPUT_TYPE_SSML "SSML"

/* Output type */
#define SYNTH_MSG_OUTPUT_TYPE "AUDIO"

/* Output value for OUTPUT_TYPE AUDIO*/
#define SYNTH_MSG_OUTPUT_AUDIO_WAVE_FILE	"WAVE_FILE"
#define SYNTH_NSG_OUTPUT_AUDIO_AU_FILE		"AU_FILE"		
#define SYNTH_MSG_OUTPUT_AUDIO_AIFF_FILE	"AIFF_FILE"

/* Locale */
#define SYNTH_MSG_LOCALE_VI		"vi"

/* Voice */
#define SYNTH_MSG_VOICE_TVH		"ttvn-vdts-hsmm"

#define SYNTH_MSG_MAX_DATA_LENGTH		524288		// 1024 * 512

/* Request templae */
#define SYNTH_MSG_REQUEST_TMP "%s:%s/process?INPUT_TYPE=%s&INPUT_TEXT=%s&OUTPUT_TYPE=%s&AUDIO=%s&LOCALE=%s&VOICE=%s"

long g_memory;		/* Debug allocate memory for audio data stream */

/* Info synth http message */
typedef struct synth_message_info 
{
	char url[64];
	char port[8];
	char input_type[32];
	char output_type[32];
	char audio[32];
	char locale[16];
	char voice[32];

} synth_message_info_t;

/* Header audio wave file */
typedef struct audio_wave_header_fmt
{
	char mix[40];
	uint32_t length;
} audio_wave_header_fmt_t;

/**
 * @Function: 		synth_http_send_request_msg
 * @Description:	
 *					Send http message 
 * @Input:			
 *					requestURL: url for request
 * @Output:			
 *					response:	response for request
 *					length:		length of response
 * @Return:	
 *					TRUE if success
 **/
apt_bool_t synth_http_send_request_msg (char *requestURL, void **response, int *length);

/**
 * @Function: 		synth_send_request_get_audio
 * @Description:	
 *					Send request to send tss server for request get audio 
 * @Input:			
 *					text_input: Content of request 
 *					msg_info:	General information of request
 * @Output:			
 *					response:	response for request
 *					length:		length of response
 * @Return:	
 *					TRUE if success
 **/
apt_bool_t synth_send_request_get_audio (char *text_input, synth_message_info_t msg_info, void *data_stream);

/**
 * @Function: 		synth_build_request_get_audio
 * @Description:	
 *					Buid request to send tss server for request get audio
 * @Input:			
 *					msg_info:	info param of request
 *					text_input: Content of request
 * @Output:			
 *					requestURL: url for request 
 * @Return:	
 *					TRUE if success
 **/
apt_bool_t synth_build_request_get_audio (char *requestURL, synth_message_info_t msg_info, char *text_input);

/**
 * @Function: 		synth_process_response_get_audio
 * @Description:	
 *					Process response
 * @Input:			
 *					receive_data:	data receive from response
 * @Output:			
 *					response:		response for request
 *					length:			length of response
 * @Return:	
 *					TRUE if success
 **/
apt_bool_t synth_process_response_get_audio (char *receive_data, void **response, int *length);

/**
 * @Function: 		synth_dispatcher_http_request
 * @Description:	
 *					Dispatcher address send request
 * @Input:			
 * @Output:			
 *					msg_info:		info param for request
 * @Return:	
 *					TRUE if success
 **/
apt_bool_t synth_dispatcher_http_request (char *addr, char *port, char *input_type, char *output_type, char *audio, char *locale, char * voice, synth_message_info_t * msg_info);

/**
 * @Function: 		read_data_from_http_message
 * @Description:	
 *					Read data from response received
 * @Input:			
 * @Output:			
 *					userdata:		
 * @Return:	
 *					size of data readed
 **/
size_t read_data_from_http_message (void *ptr, size_t size, size_t nmemb, void **userdata);


#endif /* SYNTH_HTTP_MESSAGE_H_ */
