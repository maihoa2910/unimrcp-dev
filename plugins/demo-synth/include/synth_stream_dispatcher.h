
#ifndef SYNTH_STREAM_DISPATCHER_H_
#define SYNTH_STREAM_DISPATCHER_H_

#include "pthread.h"
#include "synth_http_message.h"

#include "apt_log.h"

/* Maximum of stream to tts */
#define SYNTH_STREAM_MAX		400

pthread_mutex_t mutex;
unsigned int stream_count;

typedef struct synth_stream_data
{
	char url[5000];
	void **tts_response;
	int *len;
	void *mrcp_channel;
	void *mrcp_request;
	void *mrcp_response;
} synth_stream_data_t;

apt_bool_t (*send_http_request)(char *url, void **response, int *len);
apt_bool_t (*send_mrcp_inprogress)(void *channel, void *request, void *response);

apt_bool_t synth_init_stream_dispatcher();

void *synth_stream_send_msg(void *data);

apt_bool_t synth_stream_dispatcher(synth_stream_data_t *stream_data);


#endif /* SYNTH_STREAM_DISPATCHER_H_ */
